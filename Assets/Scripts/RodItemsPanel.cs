﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RodItemsPanel : MonoBehaviour
{
    public GameObject Content;
    public GameObject itemPrefab;
    public GameObject itemSectorPrefab;

    private Dictionary<TypeEnum, ItemSectorPanel> currentCreatedTypes;

    void Start()
    {
        currentCreatedTypes = new Dictionary<TypeEnum, ItemSectorPanel>();
        CreateRods();
    }

    private void CreateRods()
    {
        RodItemsController controller = new RodItemsController();
        string currentChoseId = PlayerPrefs.GetString(PlayerPrefsKeys.CHOSEN_ROD_KEY, "");
        foreach (ItemSO item in GameData.Instance.rodsList)
        {
            GameObject newItem = CreateItem(item, item.rodSprite);
            Button button = newItem.GetComponent<Button>();
            RodItemButton buttonScript = newItem.GetComponent<RodItemButton>();
            button.onClick.AddListener(() => controller.RodItemButtonAction(buttonScript, item));
            if (currentChoseId == item.id || currentChoseId == "")
            {
                controller.RodItemButtonAction(buttonScript, item);
            }
            
            SetItemToSector(newItem, item.type);
        }
    }

    private GameObject CreateItem(ItemSO itemSO, Sprite itemImage)
    {
        GameObject newItem = Instantiate(itemPrefab);
        newItem.GetComponent<RodItemButton>().SetInfo(itemSO, itemImage);
        return newItem;
    }

    private void SetItemToSector(GameObject item, TypeEnum type)
    {
        if (!currentCreatedTypes.ContainsKey(type))
        {
            CreateNewSector(type);
        }

        currentCreatedTypes[type].AddItemToType(item);
    }

    private void CreateNewSector(TypeEnum type)
    {
        ItemSectorPanel newSector = Instantiate(itemSectorPrefab, Content.transform).GetComponent<ItemSectorPanel>();
        currentCreatedTypes[type] = newSector;
        newSector.SetTypeName(type.ToString());
    }

    public void TurnOffAnimator()
    {
        GetComponent<Animator>().enabled = false;
    }
}
