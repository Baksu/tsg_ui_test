﻿using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable objects", menuName = "SO/new item")]
public class ItemSO : ScriptableObject
{
    public string id;
    public string itemName;
    public TypeEnum type;
    public LevelEnum level;
    public Sprite rodSprite;
    public GameObject rodModel;
}
