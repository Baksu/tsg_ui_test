﻿Shader "Unlit/Shadow"
{
    Properties {
        _Color ("Main Color", Color) = (1,1,1,1)
    }

    SubShader {
        Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        
        ZWrite Off
        ZTest Always
        Lighting Off
        Fog { Mode Off }

        Blend SrcAlpha OneMinusSrcAlpha 
        
        Stencil
        {
            Ref 1
            Comp notequal
        }

        Pass {
            Color [_Color]
        }
    }
}
