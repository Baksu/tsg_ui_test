﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class RodItemButton : MonoBehaviour
{
    public TextMeshProUGUI itemName;
    public TextMeshProUGUI progressText;
    public Slider progress;
    public TextMeshProUGUI levelText;
    public Image itemImage;
    public Image progressImage;
    public Image progressBackgroundImage;
    public Image backgroundImage;
    public Image levelBackgroundImage;
    public GameObject levelUpImage;
    public GameObject chooseBar;
    public Animator animator;
    
    public bool isChosen;
    
    private string itemId;
    private bool setColorProgress;
    
    
    public void SetInfo(ItemSO _itemSO, Sprite _itemImage)
    {
        itemId = _itemSO.id;
        itemName.text = _itemSO.itemName.ToUpper();
        itemImage.sprite = _itemImage;
        SetRandomValues();
        SetLevelGraphics(_itemSO.level);
    }

    private void SetLevelGraphics(LevelEnum level)
    {
        ItemLevelGraphicsSO graphics = GameData.Instance.GetItemLevelGraphics(level);
        progressBackgroundImage.sprite = graphics.progressBackgroundImage;
        backgroundImage.sprite = graphics.backgroundImage;
        levelBackgroundImage.sprite = graphics.levelBackgroundImage;

        if (setColorProgress)
        {
            progressImage.sprite = graphics.progressImage;
        }
    }
    
    private void SetRandomValues()
    {
        int firstValue = Random.Range(0, 101);
        int secondValue = Random.Range(0, 101);
        int levelValue = Random.Range(1, 11);

        progressText.text = firstValue + "/" + secondValue;
        if (firstValue >= secondValue)
        {
            progress.maxValue = 1;
            progress.value = 1;
            levelUpImage.SetActive(true);
            animator.enabled = true;
        }
        else
        {
            progress.maxValue = secondValue;
            progress.value = firstValue;
            setColorProgress = true;
            animator.enabled = false;
        }

        levelText.text = levelValue.ToString();
    }

    public void SetRod(bool set)
    {
        if (set)
        {
            chooseBar.SetActive(true);
            isChosen = true;
        }
        else
        {
            chooseBar.SetActive(false);
            isChosen = false;
        }
    }

    public string GetItemId()
    {
        return itemId;
    }
}
