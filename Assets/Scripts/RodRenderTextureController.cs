﻿using UnityEngine;

public class RodRenderTextureController : MonoBehaviour
{
    public GameObject rodSpawner;
    public Material shadowMat;
    
    private GameObject currentSpawnRod;
    private GameObject shadowSpawn;

    private void Awake()
    {
        RodItemsController.ChooseStickAction += SpawnNewRod;
    }

    private void OnDestroy()
    {
        RodItemsController.ChooseStickAction -= SpawnNewRod;
    }

    private void SpawnNewRod(ItemSO rod)
    {
        if (currentSpawnRod != null)
        {
            Destroy(currentSpawnRod);
            Destroy(shadowSpawn);
        }

        if (rod.rodModel != null)
        {
            currentSpawnRod = Instantiate(rod.rodModel, rodSpawner.transform, false);
            shadowSpawn = Instantiate(rod.rodModel, rodSpawner.transform, false);
            shadowSpawn.transform.localPosition = new Vector3(0, - 0.055f, -0.07f);
            shadowSpawn.GetComponentInChildren<Renderer>().material = shadowMat;
        }
    }
}
