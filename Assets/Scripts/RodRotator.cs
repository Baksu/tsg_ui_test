﻿using UnityEngine;
using UnityEngine.EventSystems;

public class RodRotator : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public GameObject rodSpawner;
    public float speedRotation = 1f;
    
    private Vector2 lastPosition;
    
    public void OnBeginDrag(PointerEventData data)
    {
        lastPosition = data.position;
    }

    public void OnDrag(PointerEventData data)
    {
        float direction = 0f;
        if (data.position.x > lastPosition.x)
        {
            direction = 1;
        }
        else if (data.position.x < lastPosition.x)
        {
            direction = -1;
        }
        rodSpawner.transform.Rotate(0, direction * speedRotation, 0, Space.World );
        lastPosition = data.position;
    }
    
    public void OnEndDrag(PointerEventData eventData)
    {
    }
    
    
}
