﻿using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour
{
    private static GameData instance;

    public static GameData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load<GameData>("GameData");
            }

            return instance;
        }
    }

    public List<ItemSO> rodsList;
    public List<ItemLevelGraphicsSO> itemsGraphics;

    private void Awake()
    {
        instance = this;
    }

    public ItemSO GetItemInfo(string id)
    {
        return rodsList.Find(r => r.id == id);
    }
    
    public ItemLevelGraphicsSO GetItemLevelGraphics(LevelEnum level)
    {
        return itemsGraphics.Find(g => g.level == level);
    }
}
