﻿using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ItemSectorPanel : MonoBehaviour
{
    public TextMeshProUGUI sectorName;
    public GameObject horizontalLayoutPrefab;

    private List<GameObject> layouts;

    private int currentItemsInLayout;
    
    public void SetTypeName(string _sectorName)
    {
       sectorName.text = _sectorName.ToUpper();
    }

    public void AddItemToType(GameObject item)
    {
        if (layouts == null)
        {
            layouts = new List<GameObject>();
        }
        GameObject currentUseLayout;
        if (currentItemsInLayout == 0)
        {
            currentUseLayout = Instantiate(horizontalLayoutPrefab, transform, false);
            layouts.Add(currentUseLayout);
        }
        else
        {
            currentUseLayout = layouts.Last();
        }
        
        item.transform.SetParent(currentUseLayout.transform, false);
        currentItemsInLayout = currentItemsInLayout == 3 ? 0 : currentItemsInLayout + 1;
    }
}
