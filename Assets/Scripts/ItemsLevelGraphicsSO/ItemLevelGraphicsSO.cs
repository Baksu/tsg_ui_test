﻿using UnityEngine;

[CreateAssetMenu(fileName = "Scriptable objects", menuName = "SO/new item level graphics")]
public class ItemLevelGraphicsSO : ScriptableObject
{
    public LevelEnum level;
    public Sprite backgroundImage;
    public Sprite levelBackgroundImage;
    public Sprite progressBackgroundImage;
    public Sprite progressImage;
}
