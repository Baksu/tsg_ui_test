﻿using System;
using UnityEngine;

public class RodItemsController
{
    public static event Action<ItemSO> ChooseStickAction;

    private static RodItemButton currentChooseRod;

    public void RodItemButtonAction(RodItemButton button, ItemSO rodItem)
    {
        if (!button.isChosen)
        {
            PlayerPrefs.SetString(PlayerPrefsKeys.CHOSEN_ROD_KEY, button.GetItemId());
            if (currentChooseRod != null)
            {
                currentChooseRod.SetRod(false);
            }
            currentChooseRod = button;
            button.SetRod(true);
            
            if (ChooseStickAction != null)
            {
                ChooseStickAction(rodItem);
            }
        }
    }
}
